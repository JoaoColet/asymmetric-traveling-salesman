/* Problema do caixeiro viajante assimétrico
*  utilizando a meta-heuristica VNS
*  Disciplina: Projeto de Algoritmos - Aluno: João Mateus Colet 
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include <stdio.h>
#include <time.h>

double PCFreq = 0.0;
__int64 CounterStart = 0;

void StartCounter()
{
    LARGE_INTEGER li;
    if(!QueryPerformanceFrequency(&li))
        printf("QueryPerformanceFrequency Failed.\n");
    PCFreq = (double)(li.QuadPart) / 1000.0;
    QueryPerformanceCounter(&li);
    CounterStart = li.QuadPart;
}
 
double GetCounter()
{
    LARGE_INTEGER li;
    QueryPerformanceCounter(&li);
    return (double)(li.QuadPart - CounterStart) / PCFreq;
}

int sorteiacidade(int dim)
{
	int i;
	i=rand() % dim;
	return i;
}

int main(int argc, char * argv[])
{
	if (argc < 2) return 1;
	char * filename = argv[1];
	FILE * fp = fopen(filename, "r");
	if (fp == NULL) return 1;
	char buf[30];
	int dim, dimnext=0, **matriz, i=0, j=0, atribui=0;
	srand(time(NULL));
	
	//----- LEITURA DO ARQUIVO -----//
	while( fscanf(fp, "%s", buf) != EOF )
    {	
		if(dimnext==1){ // le o tamanho da matriz
			dim = atoi(buf);
			matriz=(int**)calloc(dim,sizeof(int*));
			for(i=0;i<dim;i++)
				matriz[i]=(int*)calloc(dim,sizeof(int));
			dimnext=0;
			i=0;
		}
		else if(!strcmp(buf, "DIMENSION:")){ //identifica quando será lido o tamanho da matriz
			dimnext=1;
		}
		else if(!strcmp(buf, "EDGE_WEIGHT_SECTION")){ //identifica quando irá começar a matriz
			atribui=1;
		}
		else if(!strcmp(buf, "EOF")){
			atribui=0;
		}
		else if(atribui == 1){ //le os valores da matriz e os atribui na matriz do codigo
			matriz[i][j] = atoi(buf);	
			j++;
			if(j==dim){
				j=0;
				i++;
			}
		}
    }
	//------ METODO GULOSO-----//
	int passados[dim], finish=dim, local=0, menor, distgul=0, caminhogul[dim+1], c;
	double tempoEmMilissegundo = 0.0000000;
	for(i=0; i<dim; i++){ //inicializações da solução
		passados[i]=0;
		caminhogul[i]=-1;
	}
	passados[0]=1;
	caminhogul[0]=0;
	caminhogul[dim]=0;
	j=0;
	c=1;
	StartCounter();
	while(finish>1){
		menor=99999;
		for(j=0;j<dim;j++){ //adiciona uma cidade a calcula a distancia
			if((matriz[local][j]<menor) && (passados[j]==0) && (local != j)){
				menor=matriz[local][j];
				i=j;
			}			
		}
		passados[i]=1;
		distgul=distgul+menor;
		local=i;
		caminhogul[c]=i;
		c++;
		finish--;
	}
	distgul=distgul+matriz[local][0];
	tempoEmMilissegundo = GetCounter();
	
	printf("\nCaminho Guloso\nDistancia = %i\nTempo = %.7lf milissegundos\n",distgul,tempoEmMilissegundo);
	
	//------caminho Aleatorio----//
	int sort, checkcity, caminho[dim+1], melhorcaminho[dim+1], quantvolta=1, k, g, v, b, aux, aux2, aux3, dist=0, h, menordist=99999999, somadist=0;
	finish=100; //criterio de parada
	h=9999999;
	tempoEmMilissegundo = 0.0000000;
	StartCounter();
	while(finish>0){  //inicializa as variaveis
	quantvolta=1;
	dist=0;
	for(i=0; i<dim; i++){
		passados[i]=0;
		caminho[i]=-1;
	}
	caminho[0]=0;
	caminho[1]=0;
	caminho[dim]=-1;
	passados[0]=1;
		while(quantvolta<dim){   //insere vertices até q todos sejam inseridos
			sort=sorteiacidade(dim);
			checkcity=0;
			while(checkcity==0){        //verifica se não foi sorteado um vertice ja inserido
				if(passados[sort]==0){
					checkcity=1;
				}
				else{
					sort++;
					j++;
					if(sort==dim)
						sort=1;
					if(j==dim)
						checkcity=2;
				}
			}
			local=0;
			menor=99999;
			for(i=1;i<=quantvolta;i++){    //calcula a menor distancia para iserir o vertice
				aux=matriz[caminho[i-1]][sort] + matriz[sort][caminho[i]];
				if(aux<menor){
					menor=aux;
					local=i;
				}
			}
			quantvolta++;
			passados[sort]=1;
			for(i=quantvolta;i>local;i--){ //insere o vertice na melhor possição
				caminho[i]=caminho[i-1];
			}
			caminho[local]=sort;			
		}
		for(i=0;i<dim;i++){ //calcula o caminho total
			dist+=matriz[caminho[i]][caminho[i+1]];
		}
		finish--;
		if(dist<menordist)
			menordist=dist;
		
		//----------------VNS------------------//
		k=1; 
		while(k<5){ 			//kmax interações
			//-----2opt-----//
			for(i=1; i<dim;i++){
				for(j=i+1; j<dim; j++){
					g=0;
					c=0;
					aux=caminho[i];
					aux2=caminho[j];
					if(i+1==j){
						g=matriz[caminho[i-1]][aux] + matriz[aux][aux2] + matriz[aux2][caminho[j+1]];
						c=matriz[caminho[i-1]][aux2] + matriz[aux2][aux] + matriz[aux][caminho[j+1]];
					}
					else{
						g=matriz[caminho[i-1]][aux] + matriz[aux][caminho[i+1]] + matriz[caminho[j-1]][aux2] + matriz[aux2][caminho[j+1]];
						c=matriz[caminho[i-1]][aux2] + matriz[aux2][caminho[i+1]] + matriz[caminho[j-1]][aux] + matriz[aux][caminho[j+1]];
					}
					if(c<g){
						caminho[i]=aux2;
						caminho[j]=aux;
						dist=dist-g+c;
					}
				}
			}
			
			//------- 3-opt ------//
			for(h=1;h<dim-2;h++){
				for(i=h+1;i<dim-1;i++){
					for(j=i+1;j<dim;j++){
						g=0;
						c=0;
						v=0;
						aux=caminho[h];
						aux2=caminho[i];
						aux3=caminho[j];
						if(h+1==i && i+1==j){
							g=matriz[caminho[h-1]][aux] + matriz[aux][aux2] + matriz[aux2][aux3] + matriz[aux3][caminho[j+1]];
							c=matriz[caminho[h-1]][aux3] + matriz[aux3][aux] + matriz[aux][aux2] + matriz[aux2][caminho[j+1]];
							v=matriz[caminho[h-1]][aux2] + matriz[aux2][aux3] + matriz[aux3][aux] + matriz[aux][caminho[j+1]];
							if(c<v && c<g){
								caminho[h]=aux3;
								caminho[i]=aux;
								caminho[j]=aux2;
								dist=dist-g+c;
							}
							else if(v<c && v<g){
								caminho[h]=aux2;
								caminho[i]=aux3;
								caminho[j]=aux;
								dist=dist-g+v;
							}
						}
						else if(h+1==i){
							g=matriz[caminho[h-1]][aux] + matriz[aux][aux2] + matriz[aux2][caminho[i+1]] + matriz[caminho[j-1]][aux3] + matriz[aux3][caminho[j+1]];
							c=matriz[caminho[h-1]][aux3] + matriz[aux3][aux] + matriz[aux][caminho[i+1]] + matriz[caminho[j-1]][aux2] + matriz[aux2][caminho[j+1]];
							v=matriz[caminho[h-1]][aux2] + matriz[aux2][aux3] + matriz[aux3][caminho[i+1]] + matriz[caminho[j-1]][aux] + matriz[aux][caminho[j+1]];
							if(c<v && c<g){
								caminho[h]=aux3;
								caminho[i]=aux;
								caminho[j]=aux2;
								dist=dist-g+c;
							}
							else if(v<c && v<g){
								caminho[h]=aux2;
								caminho[i]=aux3;
								caminho[j]=aux;
								dist=dist-g+v;
							}
						}
						else if(i+1==j){
							g=matriz[caminho[h-1]][aux] + matriz[aux][caminho[h+1]] + matriz[caminho[i-1]][aux2] + matriz[aux2][aux3] + matriz[aux3][caminho[j+1]];
							c=matriz[caminho[h-1]][aux3] + matriz[aux3][caminho[h+1]] + matriz[caminho[i-1]][aux] + matriz[aux][aux2] + matriz[aux2][caminho[j+1]];
							v=matriz[caminho[h-1]][aux2] + matriz[aux2][caminho[h+1]] + matriz[caminho[i-1]][aux3] + matriz[aux3][aux] + matriz[aux][caminho[j+1]];
							if(c<v && c<g){
								caminho[h]=aux3;
								caminho[i]=aux;
								caminho[j]=aux2;
								dist=dist-g+c;
							}
							else if(v<c && v<g){
								caminho[h]=aux2;
								caminho[i]=aux3;
								caminho[j]=aux;
								dist=dist-g+v;
							}
						}						
						else{
							g=matriz[caminho[h-1]][aux] + matriz[aux][caminho[h+1]] + matriz[caminho[i-1]][aux2] + matriz[aux2][caminho[i+1]] + matriz[caminho[j-1]][aux3] + matriz[aux3][caminho[j+1]];
							c=matriz[caminho[h-1]][aux3] + matriz[aux3][caminho[h+1]] + matriz[caminho[i-1]][aux] + matriz[aux][caminho[i+1]] + matriz[caminho[j-1]][aux2] + matriz[aux2][caminho[j+1]];
							v=matriz[caminho[h-1]][aux2] + matriz[aux2][caminho[h+1]] + matriz[caminho[i-1]][aux3] + matriz[aux3][caminho[i+1]] + matriz[caminho[j-1]][aux] + matriz[aux][caminho[j+1]];
							if(c<v && c<g){
								caminho[h]=aux3;
								caminho[i]=aux;
								caminho[j]=aux2;
								dist=dist-g+c;
							}
							else if(v<c && v<g){
								caminho[h]=aux2;
								caminho[i]=aux3;
								caminho[j]=aux;
								dist=dist-g+v;
							}
						}
					}
				}
			}
			if(dist<menordist){ //verifica se foi encontrado um ótimo local melhor q o ótimo global
				k=1;
				menordist=dist;
			}
			else
				k++;
		}
	}
	tempoEmMilissegundo = GetCounter();
	printf("\nCaminho Aleatorio com VNS\nDistancia = %i\nTempo = %.7lf milissegundos\n",menordist,tempoEmMilissegundo);

	for(i=0;i<dim;i++)
		free(matriz[i]);
	free(matriz);
	fclose(fp);
	return 0;
}